<div id="section5">
    <div class="mt-5 margin-mobile text-center colorPrimary textBigger font-weight-bold">
        APERTE NO BOTÃO ABAIXO PARA SE INSCREVER
    </div>

    <div class="row margin-mobile">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <a href="#section1" class="btn mt-5 btnReservar btnFinal textBigger font-weight-bold">
                QUERO ME INSCREVER!
            </a>
            <div class="mt-5 text-center colorPrimary d-none d-md-block">
                <span>
                    <img src="./assets/logo_calendar.webp" alt="" class="card-img" id="logoCalendar2">
                </span>
                <span class="font-weight-bold textNormal colorPrimary">
                    De 2 a 7 de julho
                </span>
            </div>
            <div class="d-md-none text-center mt-3">
                <img src="./assets/logo_calendar.webp" alt="" class="card-img" id="logoCalendar2">
                <div class="font-weight-bold textNormal colorPrimary mt-2">
                    De 2 a 7 de julho
                </div>
            </div>
        </div>
        <div class="col-lg-4"></div>
    </div>

    <br><br><br>
</div>