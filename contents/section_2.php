<div class="mt-5 text-center colorPrimary textBigger font-weight-bold margin-mobile">
    O QUE VOCÊ VAI APRENDER NA <br>
    SEMANA DO GESTOR DE AUTOMAÇÃO:
</div>

<div class="row mt-3 margin-mobile">
    <div class="col-lg-3 col-xl-4"></div>
    <div class="col-lg-7 col-xl-7">
        <table id="tableAprender">
            <tbody>
                <tr>
                    <td class="d-none d-md-table-cell position-relative tdCircleAula">
                        <div class="circleAula text-center">
                            <span class="font-weight-bold colorSecondary textCardAprender blur">
                                AULA 01
                            </span>
                        </div>
                        <br><br><br>
                    </td>
                    <td class="tdRightCircleAula colorSecondary d-flex align-items-center juntify-content-center">
                        <div class="d-md-none mb-3 circleAula text-center">
                            <span class="font-weight-bold colorSecondary textCardAprender blur">
                                AULA 01
                            </span>
                        </div>

                        <span>
                            <img src="./assets/logo_calendar.webp" alt="" class="card-img" id="logoCalendarTable">
                            <u class="colorSecondary font-weight-bold textNormal blur">
                                2 de julho
                            </u>
                            <div class="mt-2 colorSecondary textNormal">
                                Como começar no mercado extremamente lucrativo das automações <br class="d-none d-md-block"> e ganhar de 5 a 10 mil reais por mês, mesmo que você esteja do zero.
                            </div>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="d-none d-md-table-cell position-relative tdCircleAula">
                        <div class="circleAula text-center">
                            <span class="font-weight-bold colorSecondary textCardAprender blur">
                                AULA 02
                            </span>
                        </div>
                        <br><br><br>
                    </td>
                    <td class="tdRightCircleAula colorSecondary d-flex align-items-center juntify-content-center">
                        
                        <div class="d-md-none mb-3 circleAula text-center">
                            <span class="font-weight-bold colorSecondary textCardAprender blur">
                                AULA 02
                            </span>
                        </div>

                        <span>
                            <img src="./assets/logo_calendar.webp" alt="" class="card-img" id="logoCalendarTable">
                            <u class="colorSecondary font-weight-bold textNormal blur">
                                3 de julho
                            </u>
                            <div class="mt-2 colorSecondary textNormal">
                                2 a 4 horas por dia é o suficiente para você se tornar um dos <br class="d-none d-md-block"> profissionais mais bem pagos do digital.
                            </div>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="d-none d-md-table-cell position-relative tdCircleAula">
                        <div class="circleAula text-center">
                            <span class="font-weight-bold colorSecondary textCardAprender blur">
                                AULA 03
                            </span>
                        </div>
                        <br><br><br>
                    </td>
                    <td class="tdRightCircleAula colorSecondary d-flex align-items-center juntify-content-center">
                        <div class="d-md-none mb-3 circleAula text-center">
                            <span class="font-weight-bold colorSecondary textCardAprender blur">
                                AULA 03
                            </span>
                        </div>

                        <span>
                            <img src="./assets/logo_calendar.webp" alt="" class="card-img" id="logoCalendarTable">
                            <u class="colorSecondary font-weight-bold textNormal blur">
                                4 de julho
                            </u>
                            <div class="mt-2 colorSecondary textNormal">
                            Como encontrar clientes e quanto cobrar pelo seu serviço <br class="d-none d-md-block"> para faturar pelo menos 5 mil reais nas primeiras semanas de atuação
                            </div>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="d-none d-md-table-cell position-relative tdCircleAula">
                        <div class="circleAula text-center">
                            <span class="font-weight-bold colorSecondary textCardAprender blur">
                                AULA 04
                            </span>
                        </div>
                        <br><br><br>
                    </td>
                    <td class="tdRightCircleAula colorSecondary d-flex align-items-center juntify-content-center">
                        <div class="d-md-none mb-3 circleAula text-center">
                            <span class="font-weight-bold colorSecondary textCardAprender blur">
                                AULA 04
                            </span>
                        </div>

                        <span>
                            <img src="./assets/logo_calendar.webp" alt="" class="card-img" id="logoCalendarTable">
                            <u class="colorSecondary font-weight-bold textNormal blur">
                                7 de julho
                            </u>
                            <div class="mt-2 colorSecondary textNormal">
                            O segredo para faturar de 5 a 10 mil reais por mês como Gestor de <br class="d-none d-md-block"> Automação e ser um dos profissionais mais bem pagos do digital.
                            </div>
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-lg-2 col-xl-1"></div>
</div>



<br><br>