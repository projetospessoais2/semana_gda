<div id="section2-3">
    <div class="text-center textNormal mt-4 mb-4">
        <b>Um mercado em franca expansão.</b> (precisando de pessoas como você!)
    </div>
</div>

<div class="row mt-5 position-relative margin-mobile">
    <div class="col-lg-2"></div>
    <div class="col-lg-4">

        <div class="textSmall colorSecondary">
            Na medida em que as pessoas ficam mais conectadas, as empresas precisam aumentar o investimento no mundo digital. E isso significa mais automações para crescer o faturamento dessas empresas, dessa forma, vem crescendo cada vez mais o investimento em Automações.

            <div class="mt-3 colorSecondary">
                Essa demanda é praticamente infinita e não existe concorrência porque o número de bons profissionais nesse mercado é muito pequeno! Na Semana do Gestor de Automação, você vai conhecer a fundo esse mercado e o que vai precisar para fazer parte dele, ser um profissional respeitado e muito bem remunerado.
            </div>
        </div>
    </div>
    <div class="col-lg-6"></div>

    <div class="centerMobile">
        <img src="./assets/section2_2.webp" alt="" class="card-img position-absolute robotImg">
    </div>


    <div class="mt-2 row" id="rowVenderMais">
        <div class="col-lg-2"></div>
        <div class="col-lg-4 font-weight-bold colorSecondary mt-3 mb-3 textSmall centerMobile">
            Hoje no Brasil existem mais de 21 milhões de empresas abertas no Brasil, todos esses negócios precisam vender mais e as automações são formas fáceis e rápidas de fazer isso!
        </div>
    </div>

</div>


<div class="row mt-3 mb-3 margin-mobile">
    <div class="col-lg-2"></div>
    <div class="col-lg-8 text-center textMuted textExtraSmall">
        Se tornar Gestor de Automação é uma das maiores oportunidades da atualidade, pois pode ser exercida por quem quer uma ótima remuneração em tempo integral ou no tempo livre.

        <div class="row mt-3">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <a href="#section1" class="btn btnReservar btnFinal textBigger font-weight-bold">
                    QUERO SABER MAIS SOBRE ESSA PROFISSÃO!
                </a>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </div>
    <div class="col-lg-2"></div>
    <br><br><br>
</div>