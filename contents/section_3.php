<div id="section5">
    <div class="mt-5 text-center colorPrimary textBigger font-weight-bold">
        TODAS AS SUAS DÚVIDAS SERÃO RESPONDIDAS NA AULA:
    </div>

    <div class="row mt-4 margin-mobile">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <table id="tableDuvidas" class="mt-4 d-none d-md-block">
                <tbody>
                    <tr>
                        <td class="colorSecondary textNormal font-weight-normal">
                            👉 Por que é <span class="font-weight-bold colorSecondary">tão fácil</span> prestar serviços de automação, mesmo começando do zero?
                        </td>
                        <td class="colorSecondary textNormal font-weight-normal pl-5 right">
                            👉 Como precificar seu serviço e <span class="font-weight-bold colorSecondary">ganhar de 5 a 10 mil por mês</span>, trabalhando poucas horas por dia?
                        </td>
                    </tr>
                    <tr>
                        <td class="colorSecondary textNormal font-weight-normal">
                            👉 Por que não precisa ser programador para trabalhar com automações?
                        </td>
                        <td class="colorSecondary textNormal font-weight-normal pl-5 right">
                            👉 Por que serve para quem trabalha com suporte, social media, copywriting e outras áreas?
                        </td>
                    </tr>
                    <tr>
                        <td class="colorSecondary textNormal font-weight-normal">
                            👉 Por que <span class="font-weight-bold colorSecondary">gestores de tráfego estão aumentando de 30% a 50%</span> seus faturamentos apenas adicionando as automações nos seus serviços?
                        </td>
                        <td class="colorSecondary textNormal font-weight-normal pl-5 right">
                            👉 Como aprender e já prestar os primeiros serviços em poucas semanas?
                        </td>
                    </tr>
                    <tr>
                        <td class="colorSecondary textNormal font-weight-normal">
                            👉 <span class="font-weight-bold colorSecondary">Como prospectar clientes</span> dentro e fora do mundo dos lançamentos e perpétuos?
                        </td>
                        <td class="colorSecondary textNormal font-weight-normal pl-5 right">
                            👉 <span class="font-weight-bold colorSecondary">Por que esse é o melhor momento</span> para prestar serviços de automação?
                        </td>
                    </tr>
                    <tr>
                        <td class="colorSecondary textNormal font-weight-normal">
                        </td>
                        <td class="colorSecondary textNormal font-weight-normal pl-5 right">
                           👉 Por que essa é a área que mais cresce no mundo digital atualmente e promete ser <span class="font-weight-bold colorSecondary">a mais lucrativa de 2024?</span>
                        </td>
                    </tr>
                </tbody>
            </table>
            
            <div class="d-block d-md-none colorSecondary textNormal font-weight-normal mt-4">
                👉 Por que é <span class="font-weight-bold colorSecondary">tão fácil</span> prestar serviços de automação, mesmo começando do zero?

                <div class="mt-3 colorSecondary">
                    👉 Como precificar seu serviço e <span class="font-weight-bold colorSecondary">ganhar de 5 a 10 mil por mês</span>, trabalhando poucas horas por dia?
                </div>

                <div class="mt-3 colorSecondary">
                    👉 Por que não precisa ser programador para trabalhar com automações?
                </div>

                <div class="mt-3 colorSecondary">
                    👉 Por que serve para quem trabalha com suporte, social media, copywriting e outras áreas?
                </div>

                <div class="mt-3 colorSecondary">
                    👉 Por que <span class="font-weight-bold colorSecondary">gestores de tráfego estão aumentando de 30% a 50%</span> seus faturamentos apenas adicionando as automações nos seus serviços?
                </div>

                <div class="mt-3 colorSecondary">
                    👉 Como aprender e já prestar os primeiros serviços em poucas semanas?
                </div>

                <div class="mt-3 colorSecondary">
                    👉 <span class="font-weight-bold colorSecondary">Como prospectar clientes</span> dentro e fora do mundo dos lançamentos e perpétuos?
                </div>

                <div class="mt-3 colorSecondary">
                    👉 <span class="font-weight-bold colorSecondary">Por que esse é o melhor momento</span> para prestar serviços de automação?
                </div>

                <div class="mt-3 colorSecondary">
                    👉 Por que essa é a área que mais cresce no mundo digital atualmente e promete ser <span class="font-weight-bold colorSecondary">a mais lucrativa de 2024?</span>
                </div>
            </div>

        </div>
        <div class="col-lg-2"></div>
    </div>
    <br><br><br class="d-md-none">
</div>