<div id="section1">
    <div class="row mt-4">
        <div class="col-lg-2 col-xl-4"></div>
        <div class="col-lg-8 col-xl-4 text-center">
            <img src="./assets/logo.webp" alt="" class="card-img logoImg">
        </div>
        <div class="col-lg-2 col-xl-4"></div>
    </div>

    <div class="row mt-4 margin-mobile">
        <div class="col-lg-1 col-xl-2"></div>

        <div class="text-center text-lg-start col-lg-5 mt-2">
            <span class="textLarge font-weight-light">
                Seja o profissional mais valorizado da atualidade e ganhe 
                <span class="yellow font-weight-bold">
                    no mínimo 5 mil reais por mês
                </span>
                , criando automações que você faz em questão de minutos.
            </span>
            <div class="mt-4 textSmall">
                Inscreva-se gratuitamente para ter acesso a todas as aulas da <br> <span class="font-weight-bold">Semana do Gestor de Automação.</span>
            </div>
            <div class="d-none d-lg-block mt-3">
                
                    <table>
                        <tbody>
                            <tr class="d-flex align-items-center">
                                <td>
                                    <img src="./assets/logo_yt.webp" alt="" class="card-img" id="">
                                </td>
                                <td class="ml-2 font-weight-bold textSmaller rightSection1">
                                    Ao vivo no youtube
                                </td>
                                <td class="rightSection1 calendar">
                                    <img src="./assets/logo_calendar.webp" alt="" class="card-img" id="">
                                </td>
                                <td class="textSmaller rightSection1">
                                    <span class="font-weight-bold">
                        De 2 a 7 de julho
                                    </span> <br>
                                    <span class="font-weight-light textExtraSmall">
                                        Sempre às 20h 
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- <span>
                        <img src="./assets/logo_yt.webp" alt="" class="card-img" id="logoYt">
                    </span>
                    <span class="ml-2 font-weight-bold textSmaller">
                        Ao vivo no youtube
                    </span>
                    <span>
                        <img src="./assets/logo_calendar.webp" alt="" class="card-img" id="logoCalendar">
                    </span>
                    <span class="ml-2 textSmaller">
                        <span class="font-weight-bold">
                            De 16 a 21 de janeiro 
                        </span> <br>
                        <span class="font-weight-light">
                            Sempre às 20h 
                        </span> -->
                    <!-- </span> -->
            </div>
        </div>

        <div class="col-lg-5 col-xl-3 mt-2">
            <div class="mt-4 d-lg-none"></div>
            <div class="text-center font-weight-bold textNormal">
                PREENCHA SEUS DADOS ABAIXO:
            </div>

            <div class="mt-4">
                <form method="POST" action="https://vocedecolando.activehosted.com/proc.php" id="_form_25_" class="_form _form_25 _inline-form  _dark" novalidate data-styles-version="5">
                    <input type="hidden" name="u" value="25" />
                    <input type="hidden" name="f" value="25" />
                    <input type="hidden" name="s" />
                    <input type="hidden" name="c" value="0" />
                    <input type="hidden" name="m" value="0" />
                    <input type="hidden" name="act" value="sub" />
                    <input type="hidden" name="v" value="2" />
                    <input type="hidden" name="or" value="969aa574b12b1138f397ab81096f5ca3" />

                    <input type="text" id="firstname" name="firstname" placeholder="Digite seu nome" class="form-control formInput">
                    <input type="text" id="email" name="email" placeholder="Digite seu email" class="form-control formInput mt-2">
                    <input type="phone" id="phone" name="phone" placeholder="Digite seu whatsapp" class="form-control formInput mt-2" pattern="^(\(11\) [9][0-9]{4}-[0-9]{4})|(\(1[2-9]\) [5-9][0-9]{3}-[0-9]{4})|(\([2-9][1-9]\) [5-9][0-9]{3}-[0-9]{4})$">

                    <!-- UTM TAGS -->
                    <input type="hidden" id="field[1]" name="field[1]" value="" placeholder=""/>
                    <input type="hidden" id="field[2]" name="field[2]" value="" placeholder=""/>
                    <input type="hidden" id="field[3]" name="field[3]" value="" placeholder=""/>
                    <input type="hidden" id="field[4]" name="field[4]" value="" placeholder=""/>
                    <input type="hidden" id="field[5]" name="field[5]" value="" placeholder=""/>

                    <button id="_form_25_submit" class="btn mt-3 btnReservar textNormal font-weight-bold">
                        QUERO RESERVAR MINHA VAGA
                    </button>
                </form>
            </div>
        </div>

        <div class="col-lg-1 col-xl-2"></div>
    </div>

    <table class="d-lg-none mt-2">
        <tbody>
            <tr class="d-flex align-items-center justify-content-center">
                <td>
                    <img src="./assets/logo_yt.webp" alt="" class="card-img" id="">
                </td>
                <td class="ml-2 font-weight-bold textSmaller rightSection1">
                    Ao vivo no youtube
                </td>
            </tr>
            <tr class="d-flex align-items-center justify-content-center tr-mt">
                <td class="rightSection1 calendar">
                    <img src="./assets/logo_calendar.webp" alt="" class="card-img" id="">
                </td>
                <td class="textSmaller rightSection1">
                    <span class="font-weight-bold">
                        De 2 a 7 de julho
                    </span> <br>
                    <span class="font-weight-light textExtraSmall">
                        Sempre às 20h 
                    </span>
                </td>
            </tr>
        </tbody>
    </table>

    <br><br>
</div>