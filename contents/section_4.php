<div id="section1">
    <div class="d-none d-md-block mt-5">
        <div class="row margin-mobile">
            <div class="col-lg-1"></div>
            <div class="col-lg-5 text-center text-lg-start">
                <img src="./assets/luciana.webp" alt="" class="card-img" id="imgLuciana">
            </div>
            <div class="col-lg-5 col-xl-4 mt-4">
                <div class="font-weight-bold textLarge">
                    QUEM É LUCIANA PAPINI
                </div>
                <div class="font-weight-light textSmall mt-4">
                    Especialista em estratégias de automações, mentora e criadora da comunidade com os maiores e melhores gestores de automação do país.
    
                    <div class="mt-3">
                        Há 2 anos e meio presta serviço de automação para grandes players do mercado como Cris Franklin, Dr Hussein Awada, Cozinha de Gente Moderna, Raiam Santos e Luiza Valeri.
                    </div>
                    
                    <div class="mt-3">
                        Atuou como estrategista em diversos nichos do mercado digital e já acumulou mais de R$ 7 milhões de faturamento.
                    </div>
                    
                    <div class="mt-3">
                        Encontrou nas automações a possibilidade de mudar de vida, conquistar a liberdade financeira e geográfica, ter flexibilidade de horários. 
                    </div>
                    
                    <div class="mt-3">
                        E através das automações conseguiu viver 100% do digital.
                    </div>
                    
                    <div class="mt-3">
                        Hoje possui mais de 2.000 alunos e vem transformando a vida e os negócios das pessoas que passam pelo seu método.
                    </div>
                </div>
            </div>
            <div class="col-lg-1 col-xl-2"></div>
        </div>
    </div>

    <div class="d-md-none">
        <img src="./assets/luciana.webp" alt="" class="card-img" id="imgLuciana">
    </div>
    <div class="margin-mobile mt-luciana d-md-none">
        <div class="mt-4">
            <div class="font-weight-bold textLarge">
                QUEM É LUCIANA PAPINI
            </div>
            <div class="font-weight-light textSmall mt-4">
                Especialista em estratégias de automações e Agência Parceira Oficial do Manychat.

                <div class="mt-3">
                    Há 2 anos presta serviço de automação e já atendeu grandes players do mercado como Cris Franklin, Raiam Santos, Cozinha de Gente Moderna, Dr Hussein Awada, Luiza Valeri, entre outros.
                </div>
                
                <div class="mt-3">
                    Atuou como estrategista em diversos nichos do mercado digital e suas automações já foram responsáveis por mais de R$ 10 milhões de faturamento.
                </div>
                
                <div class="mt-3">
                    Encontrou no mercado digital a possibilidade de mudar de vida, conquistar a liberdade financeira e geográfica, ter flexibilidade de horários.
                </div>
                
                <div class="mt-3">
                    E através das automações conseguiu viver 100% do digital.
                </div>
                
                <div class="mt-3">
                    Hoje possui mais de 1.300 alunos e vem transformando a vida e os negócios das pessoas que passam pelo seu método.
                </div>
            </div>
        </div>
    </div>

    <br><br>
</div>