<!DOCTYPE html>
<html lang="pt-BR">

    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MS3VJCS');</script>
        <!-- End Google Tag Manager -->
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="initial-scale=1" />

        <link rel="icon" type="image/x-icon" href="assets/favicon.ico">
        <title>Semana GDA</title>

        <!-- CSS -->
        <link rel="stylesheet" href="css/style_ty.css">
    </head>

    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MS3VJCS"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <div class="row mt-4">
            <div class="col-lg-2 col-xl-4"></div>
            <div class="col-lg-8 col-xl-4 text-center">
                <img src="./assets/logo.webp" alt="" class="card-img logoImg">
            </div>
            <div class="col-lg-2 col-xl-4"></div>
        </div>

        <div class="mt-5 mb-5 margin-mobile text-center">
            <div class="textBigger font-weight-bold">
                Sua inscrição <span class="yellow font-weight-bold">NÃO</span> está concluída!
            </div>

            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="mt-4 textNormal">
                        Faltam dois passos super rápidos. O primeiro deles é entrar no seu e-mail  onde enviei informações importantes sobre a Semana do Gestor de Automação.
                    </div>

                    <div class="mt-4 textNormal">
                        O segundo passo é entrar no grupo exclusivo de whatsapp do evento. Ele será o meu principal meio de comunicação com você. 
                    </div>

                    <div class="mt-4 textNormal">
                        Aperte no botão abaixo e entre agora no grupo para concluir sua inscrição.
                    </div>
                    
                    <div class="row mt-4">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <a href="https://social.lucianapapini.com.br/entrar-no-grupo" class="btn btnReservar btnFinal textBigger font-weight-bold">
                                ENTRAR NO GRUPO
                            </a>
                        </div>
                        <div class="col-lg-3"></div>
                    </div>

                    <hr class="mt-5 mb-5">

                </div>
                <div class="col-lg-2"></div>
            </div>

            <div class="textBigger font-weight-bold">
                DEU ERRO AO ENTRAR NO GRUPO?
            </div>

            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="mt-4 textNormal">
                        Aperte no botão vermelho abaixo que vou enviar o link do grupo no privado para você. Estar neste grupo é fundamental para receber informações, links e tudo o que vai rolar na Semana do Gestor de Automação.
                    </div>


                    <div class="row mt-4">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <a href="https://social.lucianapapini.com.br/grupo-wpp" class="btn btnReservar redBtn btnFinal textBigger font-weight-bold">
                                RECEBER O LINK NO PRIVADO
                            </a>
                        </div>
                        <div class="col-lg-3"></div>
                    </div>

                </div>
                <div class="col-lg-2"></div>
            </div>

        </div>
    </body>

    <footer style="background-color: rgb(24,27,34)">
        <div class="mt-3 mb-3 text-center">
            Todos direitos reservados.
        </div>

    </footer>
</html>