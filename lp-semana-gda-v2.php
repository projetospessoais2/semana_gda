<!DOCTYPE html>
<html lang="pt-BR">

    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MS3VJCS');</script>
        <!-- End Google Tag Manager -->

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="initial-scale=1" />

        <link rel="icon" type="image/x-icon" href="assets/favicon.ico">
        <title>Semana GDA</title>

        <!-- CSS -->
        <link rel="stylesheet" href="css/used.css">

        <!-- <link rel="stylesheet" href="css/fonts.css">
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/form.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        
        <link rel="stylesheet" href="css/responsividade.css"> -->
    </head>

    <body style="background-color: white">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MS3VJCS"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <?php require('contents/section_1_v2.php'); ?>
        <?php require('contents/section_2.php'); ?>
        <?php require('contents/section_3.php'); ?>
        <?php require('contents/section_4.php'); ?>
        <?php require('contents/section_5.php'); ?>

        <script src="js/activeCampaign.js"></script>

    </body>

    <?php require('default/footer.php'); ?>

</html>